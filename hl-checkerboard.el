;;; hl-checkerboard.el --- Highlight every second sentence in a buffer  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Marty Hiatt <martianhiatus AT riseup.net>
;; Author: Marty Hiatt <martianhiatus AT riseup.net>
;; Keywords: convenience, highlighting, sentences, wp
;; Version: 0.3
;; URL: https://codeberg.org/martianh/hl-checkerboard
;; Package-Requires: ((emacs "29.1"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Highlight every second sentence in a buffer. Uses overlays for now.

;; With `hl-checkerboard-highlight-by-clause' enabled, highlight every second
;; clause. Overlays update as you type.

;; For sophisticated sentence-ending rules, set `hl-checkerboard-use-sentex' to t.

;;; Code:

(require 'org-element)
(require 'sentex nil :no-error)
(require 'clause nil :no-error)

(defgroup hl-checkerboard nil
  "Highlight every second sentence or clause in a buffer."
  :group 'convenience)

;; TODO: handle lists properly, + nested lists, URLs in lists

;;;###autoload
(defface hl-checkerboard-highlight '((t :inherit tooltip))
  ;; inheriting `highlight' is too weak in gruvbox-dark.
  "The face used to highlight every second sentence."
  :group 'hl-checkerboard)

(defcustom hl-checkerboard-use-sentex nil
  "Whether to use `sentex' to determine sentence ending.
Note that sentex is language based. To set the language it uses,
call `sentex-set-language-for-buffer'.
\nFor now, `sentex' is not performant enough for use in hl-checkerboard.el."
  :group 'hl-checkerboard
  :type 'boolean)

(defcustom hl-checkerboard-skip-org-elements t
  "Whether org elements should be skipped.
Elements include drawers, headings, comments, keywords and blocks."
  :group 'hl-checkerboard
  :type 'boolean)

(defcustom hl-checkerboard-newlines-end-sentences nil
  ;; FIXME: doesn't work with filled text. Workaround: enable
  ;; use-hard-newlines, then check for text prop 'hard after running
  ;; forward-sentence, if no 'hard 't, call forward-sentence again because
  ;; assume that we are in a filled paragraph
  ;; (use-hard-newlines 1 t)
  "Whether newlines should count as ending sentences.
Useful for org-lists, or or any other text only separated by
newlines. Note that this is not compatible with filled text!"
  :group 'hl-checkerboard
  :type 'boolean)

(defcustom hl-checkerboard-highlight-by-clause nil
  "Whether to highlight by clause rather than sentence.
A clause can be separated by comma, semi-colon, or colon, as well
as `hl-checkerboard-sentence-end-base'."
  :type 'boolean
  :group 'hl-checkerboard)

;; emacs defaults, to avoid user config by default
;; obtained with (car (get 'sentence-end-base 'standard-value))
;; maybe sentence-end emacs default could be optional?
(defcustom hl-checkerboard-sentence-end-base "[.?!…‽][]\"'”’)}»›]*"
  "Regex matching end of a sentence, not including space.
Our version of `sentence-end-base'.
Defaults to Emacs' default value, not user customized value."
  :type 'string
  :group 'hl-checkerboard)

(defcustom hl-checkerboard-sentence-end-without-space "。．？！"
  "Our version of `sentence-end-base-without-space'.
Defaults to Emacs' default value, not user customized value."
  :type 'string
  :group 'hl-checkerboard)

;; FIXME: if you change major mode, it becomes impossible to remove overlays
(defvar-local hl-checkerboard-overlays nil
  "List of all hl-checkerboard overlays.")

(defvar hl-checkerboard-forward-sentence-function nil)

(defvar hl-checkerboard-backward-sentence-function nil)

(defun hl-checkerboard--forward-sentence-function ()
  "Return the value of `hl-checkerboard-forward-sentence-function'."
  (if hl-checkerboard-use-sentex
      'sentex-forward-sentence
    'forward-sentence))

(defun hl-checkerboard--backward-sentence-function ()
  "Return the value of `hl-checkerboard-backward-sentence-function'."
  (if hl-checkerboard-use-sentex
      'sentex-backward-sentence
    'backward-sentence))

(defun hl-checkerboard--begin-pos ()
  "Return the beginning of the sentence at point."
  (save-excursion
    (unless (= (point) (point-max))
      ;; in case we just inserted white space at begin of sentece/para,
      ;; (start-of-paragraph-text) doesn't work in this case either:
      (skip-chars-forward " \t\n")
      (forward-char))
    (hl-checkerboard--backward-sentence-or-clause)
    (point)))

(defun hl-checkerboard--end-pos ()
  "Return the end of the current sentence at point."
  (save-excursion
    (unless (= (point) (point-max))
      (forward-char))
    (hl-checkerboard--backward-sentence-or-clause)
    (hl-checkerboard--forward-sentence-or-clause)
    (point)))

(defun hl-checkerboard--concat-s-end-base-org-fn ()
  "Add our simplified org-footnote re to sentence-end-base/-clause."
  (concat (if hl-checkerboard-highlight-by-clause
              (clause--sentence-end-base-clause-re)
            hl-checkerboard-sentence-end-base)
          "\\("
          "\\[fn:[-_[:word:]]+\\]"
          "\\)?"))

(defun hl-checkerboard--forward-clause (&optional arg)
  "Move forward to beginning of next clause.
With ARG, do this that many times."
  ;; ensure clause follows our sentex setting:
  (if hl-checkerboard-use-sentex
      (let ((clause-use-sentex t))
        (clause-forward-clause arg))
    (let ((clause-use-sentex nil))
      (clause-forward-clause arg))))

(defun hl-checkerboard--backward-clause (&optional arg)
  "Move backward to beginning of current clause.
With ARG, do this that many times."
  ;; ensure clause follows our sentex setting:
  (if hl-checkerboard-use-sentex
      (let ((clause-use-sentex t))
        (clause-backward-clause arg))
    (let ((clause-use-sentex nil))
      (clause-backward-clause arg))))

(defun hl-checkerboard--forward-sentence-or-clause ()
  "Move forward by clause or sentence."
  (let ((hl-checkerboard-forward-sentence-function
         (hl-checkerboard--forward-sentence-function)))
    (if hl-checkerboard-highlight-by-clause
        (hl-checkerboard--forward-clause)
      (apply hl-checkerboard-forward-sentence-function '()))))

(defun hl-checkerboard--backward-sentence-or-clause ()
  "Move backward by clause or sentence."
  (let ((hl-checkerboard-backward-sentence-function
         (hl-checkerboard--backward-sentence-function)))
    (if hl-checkerboard-highlight-by-clause
        (hl-checkerboard--backward-clause)
      (apply hl-checkerboard-backward-sentence-function '()))))

(defun hl-checkerboard-every-second-sentence ()
  "Highlight every second sentence, using overlays."
  (interactive)
  (let ((sentence-end-base (hl-checkerboard--concat-s-end-base-org-fn))
        (sentence-end-without-space hl-checkerboard-sentence-end-without-space)
        ;; to make newlines-end work, we have to set sentence-end itself:
        (sentence-end (when hl-checkerboard-newlines-end-sentences
                        (concat hl-checkerboard-sentence-end-base
                                "\\|\n"))))
    (when (> (buffer-size) 0)
      ;; TODO: make remove-overlays more specific
      (hl-checkerboard-remove-overlays)
      (save-excursion
        (if (equal major-mode 'org-mode)
            ;; FIXME: using `org-fold-core-save-visibility' here removes our
            ;; overlays!
            (org-with-wide-buffer
             ;; (org-fold-core-save-visibility nil
             (org-fold-show-all '(headings))
             (hl-checkerboard--set-initial-position)
             (hl-checkerboard--run-over-sentences))
          (hl-checkerboard--set-initial-position)
          (hl-checkerboard--run-over-sentences))))))

(defun hl-checkerboard--set-initial-position ()
  "Place point at beginning of buffer text to be highlighted."
  (goto-char (point-min))
  ;; Set initial position:
  (if (not (and (equal major-mode 'org-mode)
                hl-checkerboard-skip-org-elements))
      ;; FIXME: doesn't work if text is on first line!
      (hl-checkerboard--start-of-next-para)
    ;; move to first line after file headers and/or first org heading:
    ;; if already at heading, ie when narrowed to subtree
    (cond ((looking-at org-outline-regexp) ; "^\\*+")
           (hl-checkerboard--start-of-next-para))
          ;; move to first heading if there is one
          ((save-excursion (re-search-forward org-outline-regexp nil t))
           (org-next-visible-heading 1)
           (hl-checkerboard--start-of-next-para))
          ;; or move to after first blank line following title
          (t
           (let ((case-fold-search t))
             (when
                 (re-search-forward "^\\#\\+title" nil t)
               (hl-checkerboard--start-of-next-para)))))))

(defun hl-checkerboard--run-over-sentences ()
  "Iterate over buffer sentences, adding overlays to every second one."
  (while
      (and (< (point) (point-max))
           ;; handle last sentence in buffer followed by blank line.
           ;; if we don't do this, the last sentence will always be
           ;; highlighted because after the end of it we are not at point-max
           ;; and can call forward-sentence, but then any overlay will still
           ;; be applied to the previous sentence.
           (save-excursion
             (hl-checkerboard--forward-sentence-or-clause)
             (< (point) (point-max))))
    (when (and (equal major-mode 'org-mode)
               hl-checkerboard-skip-org-elements)
      (while (hl-checkerboard--looking-at-org-item-or-newline)
        (hl-checkerboard--move-past-org-item)))
    (hl-checkerboard--make-overlay)
    (hl-checkerboard--forward-sentence-or-clause)
    (hl-checkerboard--next-overlay-sentence)))

(defun hl-checkerboard--next-overlay-sentence ()
  "Categorically place point at beginning of the sentence after the next.
\nReady for next loop, whether we hit any paragraph ends or not."
  (unless (= (point) (point-max))
    ;; if at end of para, go to start of next, then call forward-sentence + -char
    (if (looking-at "[ ]?*\n") ; poss space at end of para
        (progn
          (hl-checkerboard--start-of-next-para)
          ;; maybe next para is an org-element in need of skipping:
          (if (hl-checkerboard--looking-at-org-item-or-newline)
              (hl-checkerboard--move-past-org-item)
            ;; else move past first sentence of new para:
            (hl-checkerboard--forward-sentence-or-clause)
            ;; mayb we hit end of buffer:
            (unless (= (point) (point-max))
              ;; forward-sentence took us to end of a list:
              (if (looking-at "[ ]?*\n")
                  (hl-checkerboard--start-of-next-para)
                (forward-char))))) ; or forward-whitespace also?
      ;; else just next sentence
      (hl-checkerboard--forward-sentence-or-clause)
      ;; mayb we hit end of buffer:
      (unless (= (point) (point-max))
        ;; if again at end of para, go to next, no need for anything further:
        (if (looking-at "[ ]?*\n") ; poss space at end of para
            (if (hl-checkerboard--looking-at-org-item-or-newline)
                (hl-checkerboard--move-past-org-item)
              (hl-checkerboard--start-of-next-para))
          ;; not at end of para: else just to go beginning of next sentence
          (when (looking-at "[[:space:]]")
            (forward-whitespace 1)))))))

;; TODO: consider using `org-end-of-meta-data':
(defun hl-checkerboard--move-past-org-item ()
  "If point at an org item, move on."
  (let ((case-fold-search t))
    (cond ((looking-at org-drawer-regexp) ; "^:.*:") ; org-drawer
           (org-forward-element))
          ((looking-at org-block-regexp) ; "^#\\+begin") ; org-block
           (org-forward-element))
          ((looking-at org-link-any-re)
           (re-search-forward org-link-any-re))
          ((while (or (looking-at org-keyword-regexp) ;"^#\\+") ; org-meta-line
                      (looking-at org-outline-regexp) ; "^\\*+") ; org-heading
                      (looking-at org-comment-regexp) ;"^#\ ") ; org-comment
                      ;; (looking-at "") ; line-feed
                      (looking-at "\n"))
             (forward-line))))))

(defun hl-checkerboard--looking-at-org-item-or-newline ()
  "Check if we are at an org item or newline."
  (or (looking-at org-drawer-regexp) ; "^:.*:") ; org-drawer
      (looking-at org-outline-regexp) ; "^\\*+") ; org-heading
      (looking-at org-block-regexp) ; "^#\\+begin") ; org-block
      (looking-at org-keyword-regexp) ; "^#\\+") ; org-meta-line
      (looking-at org-comment-regexp) ; "^#\ ") ; org-comment
      (looking-at org-link-any-re) ; org-link
      ;; (looking-at "") ; line-feed
      (looking-at "\n")))

;; TODO: use `forward-paragraph' + `forward-char'?
(defun hl-checkerboard--start-of-next-para ()
  "Move to start of next paragraph if we are at end of a paragraph."
  (unless (looking-at "[ ]?*\n")
    (forward-line)) ; move to blank line
  (while (looking-at "[ ]?*\n") ; poss end of para space
    (forward-char)))

(defun hl-checkerboard--make-overlay ()
  "Add a highlight overlay to the current sentence."
  (let ((ov (make-overlay
             (hl-checkerboard--begin-pos)
             (hl-checkerboard--end-pos)
             (current-buffer))))
    (overlay-put ov 'face 'hl-checkerboard-highlight)
    (push ov hl-checkerboard-overlays))
  (setq hl-checkerboard-overlays
        (reverse hl-checkerboard-overlays)))

(defun hl-checkerboard-remove-overlays ()
  "Remove our current overlays."
  (interactive)
  (mapc (lambda (ov)
          (delete-overlay ov))
        hl-checkerboard-overlays)
  (setq hl-checkerboard-overlays nil))

(defun hl-checkerboard-post-insert-function ()
  "Update highlighting after insertion of sentence-ending characters.
Sentence-ending characters are those in
`hl-checkerboard-sentence-end-base'."
  (hl-checkerboard-every-second-sentence))

;;;###autoload
(define-minor-mode hl-checkerboard-mode
  "Enable highlighting of every second sentence or clause."
  :init-value nil
  :lighter " hl-ckb"
  (if hl-checkerboard-mode
      (progn (add-hook 'post-self-insert-hook
                       #'hl-checkerboard-post-insert-function
                       nil t)
             (hl-checkerboard-every-second-sentence))
    (hl-checkerboard-remove-overlays)
    (remove-hook 'post-self-insert-hook
                 #'hl-checkerboard-post-insert-function t)))

(provide 'hl-checkerboard)
;;; hl-checkerboard.el ends here
